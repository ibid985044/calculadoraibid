﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Aula1WebForm1.aspx.cs" Inherits="ProjetoWebFormsVBootcamp2023.WebForm1" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br">
<head runat="server">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Calculadora com DropDownList</title>
    <link href="Style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <div id="container">
        <h1>CALCULADORA</h1>
        <form id="tipsForm" runat="server">
            <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="VALOR 1:"></asp:Label>
&nbsp;<asp:TextBox ID="txtNumber1" runat="server" CssClass="txtNumber" type="number" required></asp:TextBox><br /><br />
            <asp:DropDownList ID="ddlOperator" runat="server">
                <asp:ListItem Text="+" Value="add"></asp:ListItem>
                <asp:ListItem Text="-" Value="subtract"></asp:ListItem>
                <asp:ListItem Text="x" Value="multiply"></asp:ListItem>
                <asp:ListItem Text="÷" Value="divide"></asp:ListItem>
            </asp:DropDownList><br /><br />
            <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="VALOR 2:"></asp:Label>
            <asp:TextBox ID="txtNumber2" CssClass="txtNumber" runat="server" type="number" required></asp:TextBox><br /><br />
            <asp:Button ID="btnCalculate" runat="server" Text="CALCULAR" OnClick="btnCalculate_Click" Font-Bold="True" /><br /><br />
            <asp:Label ID="lblResult" runat="server"></asp:Label>
        </form>
    </div>
</body>
</html>
