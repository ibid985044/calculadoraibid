﻿//------------------------------------------------------------------------------
// <gerado automaticamente>
//     Esse código foi gerado por uma ferramenta.
//
//     As alterações ao arquivo poderão causar comportamento incorreto e serão perdidas se
//     o código for recriado
// </gerado automaticamente>
//------------------------------------------------------------------------------

namespace ProjetoWebFormsVBootcamp2023
{


    public partial class WebForm1
    {

        /// <summary>
        /// Controle tipsForm.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm tipsForm;

        /// <summary>
        /// Controle Label1.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label1;

        /// <summary>
        /// Controle txtNumber1.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtNumber1;

        /// <summary>
        /// Controle ddlOperator.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList ddlOperator;

        /// <summary>
        /// Controle Label2.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label2;

        /// <summary>
        /// Controle txtNumber2.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtNumber2;

        /// <summary>
        /// Controle btnCalculate.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnCalculate;

        /// <summary>
        /// Controle lblResult.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblResult;
    }
}
