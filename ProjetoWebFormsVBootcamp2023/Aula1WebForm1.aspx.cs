﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProjetoWebFormsVBootcamp2023
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnCalculate_Click(object sender, EventArgs e)
        {
            if (double.TryParse(txtNumber1.Text, out double number1) && double.TryParse(txtNumber2.Text, out double number2))
            {
                string operatorValue = ddlOperator.SelectedValue;
                double result = 0;

                switch (operatorValue)
                {
                    case "add":
                        result = number1 + number2;
                        break;
                    case "subtract":
                        result = number1 - number2;
                        break;
                    case "multiply":
                        result = number1 * number2;
                        break;
                    case "divide":
                        if (number2 != 0)
                        {
                            result = number1 / number2;
                        }
                        else
                        {
                            Response.Write("<script> alert('Erro: Divisão por zero não é permitida.');</script>");
                        }   
                        break;
                }

                lblResult.Text = "Resultado: " + result;
            }
            else
            {
                lblResult.Text = "Entrada inválida. Certifique-se de que os números são válidos.";
            }
        }
    }
}